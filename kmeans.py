# code credits to: https://pythonprogramminglanguage.com/kmeans-text-clustering/


from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
import pandas as pd
import numpy as np


# The "documents" value should look like a Python List:
"""
documents = ["This little kitty came to play when I was eating at a restaurant.",
             "Merley has the best squooshy kitten belly.",
             "Google Translate app is incredible.",
             "If you open 100 tab in google you get a smiley face.",
             "Best cat photo I've ever taken.",
             "Climbing ninja cat.",
             "Impressed with google map feedback.",
             "Key promoter extension for Google Chrome."]
"""


    # 1. Read from a csv
df_csv = pd.read_csv('datasets\Youtube comments12.csv', delimiter=',', header=0, names=['id','comment'])

"""
# Use header=0 when no column names are present in the csv file; inserts additional header row
# For giving column names:-  names=['a','b','c']
# Use 'datasets\Youtube comments12.csv'
"""


	# 2. Create a Python List 
documents = df_csv['comment'].tolist()
	
	# 3. Train the sklearn model with these "documents"/comments
vectorizer = TfidfVectorizer(stop_words='english')
X = vectorizer.fit_transform(documents)

    # *** The k-means part ***
#TODO: We are missing the Elbow method to decide True value for "k" !!
	
true_k = 2
model = KMeans(n_clusters=true_k, init='k-means++', max_iter=100, n_init=1)
model.fit(X)

print("===================\nTop terms per cluster:")
order_centroids = model.cluster_centers_.argsort()[:, ::-1]
terms = vectorizer.get_feature_names()
for i in range(true_k):
    print("Cluster %d:" % i),
    for ind in order_centroids[i, :10]:
        print(' %s' % terms[ind]),
    print
	
#output to csv file a table of the top terms under each cluster for later reference
#pd.write_csv(test1.csv)

np.savetxt(r'np.txt', df_csv['comment'].to_string(), fmt='%s') # !mistake
	
print("===================\n")
print("Prediction")


# ****
#TODO: loop to output predictions into "Cluster" column

"""
for i, row in document:
    Y = vectorizer.transform([ <row> ])
    prediction = model.predict(Y)
    #print(prediction)
    # write prediction to csv cluster as interger

"""

Y = vectorizer.transform(["A explanation."])
prediction = model.predict(Y)
print(prediction)

Y = vectorizer.transform(["Thanks I want to learn more from this course."])
prediction = model.predict(Y)
print(prediction)

